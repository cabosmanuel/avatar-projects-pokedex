$(document).ready(function () {
  var nPkms = 150; // Número de pokemones visibles
  
  // LISTAR POKEMONS -----------------------------------------------------------------------------------------------------------------------
  $.ajax({
    url: 'https://pokeapi.co/api/v2/pokemon/?limit=' + nPkms,
    type: "GET",
    dataType: "json",
    success: function (pkms) {
      
      $.each(pkms.results, function(index, value) { // Iterar pokemons
        var id = index+1;
        var name = value.name;
        
        $('#pokemons').append( // Agregar card con imagen, nombre y id
          '<div class="col">' +
            '<div class="card" href>' +
              '<a href="/pokemon/'+id+'">' +
                '<img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + id + '.png" class="card-img-top">' +
              '</a>' +
              '<div class="card-body">' +
                '<div class="row"><h5 class="card-title">[' + id + '] ' + name.toUpperCase() + '</h5></div>' +
                '<div id="types' + id + '" class="row mx-1"></div>' +
              '</div>' +
            '</div>' +
          '</div>'
        );

        if (index == pkms.results.length-1) // Cuando termine de iterar
          loadMoreInfo()
      });
    },
  });

  // CARGAR MÁS INFORMACIÓN --------------------------------
  function loadMoreInfo(){
    for (let i = 1; i <= nPkms; i++) { // Para todos los pokemones listados
      $.ajax({
        url: "https://pokeapi.co/api/v2/pokemon/" + i,
        type: "GET",
        dataType: "json",
        success: function (pkm) {

          var divTypes = '';
          $.each(pkm.types, function(index, value) { // Iterar y almacenar que tipo de pokemon es
            divTypes += 
            '<div class="col-auto badge badge-' + value.type.name + ' rounded-pill me-1">' + value.type.name + '</div>';
          });

          $('#types' + i).append(divTypes); // Agregar tipo a "card-body" de cada card
        }
      });
    }
  }

});
