$(document).ready(function () {

  // Recuperar id y cargar imagen
  var id = $('#id').text();
  $('#pkm_img').attr('src', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + id + '.png');

  // CARGAR DATOS ADICIONALES POR ID ---------------------------------------------------------------------------
  $.ajax({
    url: 'https://pokeapi.co/api/v2/pokemon/' + id,
    type: "GET",
    dataType: "json",
    success: function (pkm) {
      
      $('#name').text(pkm.name.toUpperCase()); // Nombre
      
      $.each(pkm.types, function(index, value) { // Tipos
        $('#types').append(
          '<div class="col-auto badge badge-' + value.type.name + ' rounded-pill">' + value.type.name + '</div>'
        );
      });

      $.each(pkm.stats, function(index, value) { // Stats en forma de barra
        
        var porcentage = Math.round(value.base_stat * 100 / 255);

        $('#stats').append(
          '<div id="' + value.stat.name + '" class="row">' +
            '<div class="col-3">' + value.stat.name.toUpperCase() + '</div>' +
            '<div class="col-1">' + value.base_stat + '</div>' +
            '<div class="col-6">' +
              '<div class="progress">' +
                '<div class="progress-bar" role="progressbar" style="width: '+ porcentage +'%;"' +
                'aria-valuenow="' + porcentage + '" aria-valuemin="0" aria-valuemax="100">' + porcentage + '%</div>' +
              '</div>' +
            '</div>' +
          '</div>'
        );
      })
      
    }
  });

  

});