package com.pokedex.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class PokemonController {

	@GetMapping("/pokemon/{id}")
	public String getPokemon(@PathVariable int id, Model model) {
		model.addAttribute("id", id);
		return "pokemon";
	}
}
