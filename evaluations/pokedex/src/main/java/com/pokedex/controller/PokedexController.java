package com.pokedex.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PokedexController {

	@GetMapping("/pokedex")
	public String pokedex() {
		return "pokedex";
	}
}
